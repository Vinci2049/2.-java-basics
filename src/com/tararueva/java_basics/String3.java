package com.tararueva.java_basics;

public class String3 {

    /* Given a string, count the number of words ending in 'y' or 'z' --
    so the 'y' in "heavy" and the 'z' in "fez" count, but not the 'y' in "yellow" (not case sensitive).
    We'll say that a y or z is at the end of a word if there is not an alphabetic letter immediately following it.
    (Note: Character.isLetter(char) tests if a char is an alphabetic letter.)

    countYZ("fez day") → 2
    countYZ("day fez") → 2
    countYZ("day fyyyz") → 2    */

    public int countYZ(String str) {

        char lastSym = 'a';
        int quantity = 0;

        for (int i = 0; i < str.length(); i++) {
            char curSym = str.charAt(i);
            if (Character.isLetter(curSym)) {
                lastSym = curSym;
            }
            else if (lastSym == 'y' || lastSym == 'z' || lastSym == 'Y' || lastSym == 'Z') {
                quantity++;
                lastSym = 'a';
            }
        }

        if (lastSym == 'y' || lastSym == 'z' || lastSym == 'Y' || lastSym == 'Z') {
            quantity++;
        }

        return quantity;

    }

    /* We'll say that a lowercase 'g' in a string is "happy"
    if there is another 'g' immediately to its left or right.
    Return true if all the g's in the given string are happy.

    gHappy("xxggxx") → true
    gHappy("xxgxx") → false
    gHappy("xxggyygxx") → false */

    public boolean gHappy(String str) {

        if (str.length() == 0) {
            return true;
        }

        boolean wasHappy = false;
        boolean wasUnhappy = false;
        boolean wasG = false;
        int countG = 0;

        for(int i = 0; i < str.length(); i++) {
            if (str.charAt(i) == 'g') {
                countG++;
            }
            else if (countG > 1) {
                wasHappy = true;
                countG = 0;
            }
            else if (countG == 1) {
                wasUnhappy = true;
                countG = 0;
            }
        }
        if (countG == 1) {
            wasUnhappy = true;
        }
        else if (countG > 1) {
            wasHappy = true;
        }
        return wasHappy && !wasUnhappy;
    }

}
